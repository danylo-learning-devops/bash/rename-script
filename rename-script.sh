#!/bin/bash
#Rename all files in current folder, add today date at YYYY-MM-DD format

todayDate=$(date '+%Y-%m-%d')
for file in *.jpg
do
mv "$file" "$todayDate.$file"
done
